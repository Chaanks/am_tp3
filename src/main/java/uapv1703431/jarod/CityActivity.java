package uapv1703431.jarod;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;

    CityActivity getActivity() {
        return this;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        imageWeatherCondition = findViewById(R.id.imageView);
        city = (City) getIntent().getParcelableExtra("TAG");

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);


        updateView();
        final Button but = (Button) findViewById(R.id.button);


        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    InputStream is;
                    URL a = WebServiceUrl.build(city.getName(), city.getCountry());
                    GetData gd = new GetData(city, getActivity());
                    gd.execute(a.toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        //intent = new Intent(v.getContext(), MainActivity.class);
        intent.putExtra("city", city);
        setResult(Activity.RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    void updateView() {

        try {
            textCityName.setText(city.getName());
            textCountry.setText(city.getCountry());
            textTemperature.setText(city.getTemperature() + " °C");
            textHumdity.setText(city.getHumidity() + " %");
            textWind.setText(city.getFullWind());
            textCloudiness.setText(city.getHumidity() + " %");
            textLastUpdate.setText(city.getLastUpdate());

            if (city.getIcon() != null && !city.getIcon().isEmpty()) {
                Log.d(TAG, "icon=" + "icon_" + city.getIcon());

                int icon = getResources()
                        .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, getPackageName());


                Log.d("icon id", String.valueOf(icon));
                imageWeatherCondition.setImageResource(icon);

                imageWeatherCondition.setContentDescription(city.getDescription());
            }
            Toast toast = Toast.makeText(getApplicationContext(),"Ville mise à jour avec succès", Toast.LENGTH_LONG);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                try {
                    InputStream is;
                    URL a = WebServiceUrl.build(city.getName(), city.getCountry());
                    GetData gd = new GetData(city, getActivity());
                    gd.execute(a.toString());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                return false;
        }
    }
}
