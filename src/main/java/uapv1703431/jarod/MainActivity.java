package uapv1703431.jarod;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    int RCODE_NEW_CITY_OK = 1;
    int RESULT_MAJ_CITY = 2;
    WeatherDbHelper dbHelper = new WeatherDbHelper(this);
    SimpleCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper.populate();
        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllCities(),
                new String[] {WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY},
                new int[] {android.R.id.text1, android.R.id.text2});

        ListView lv = findViewById(R.id.view);
        lv.setAdapter(cursorAdapter);

        lv.setAdapter(cursorAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent MyIntent;
                MyIntent = new Intent(view.getContext(), CityActivity.class);
                final City city = dbHelper.getCity(id);
                MyIntent.putExtra("TAG", city);
                startActivityForResult(MyIntent, RESULT_MAJ_CITY);
            }
        });


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent;
                intent = new Intent(view.getContext(), NewCityActivity.class);
                startActivityForResult(intent,RCODE_NEW_CITY_OK);
            }
        });
    }


    @Override
    protected void onActivityResult(int rcode, int rescode, Intent data){
        super.onActivityResult(rcode, rescode, data);
        if (rcode == RCODE_NEW_CITY_OK){

            if (data != null) {
                String new_city_country = data.getStringExtra("Country");
                String new_city_name = data.getStringExtra("Name");
                City newCity = new City(new_city_name, new_city_country);
                dbHelper.addCity(newCity);
                cursorAdapter.changeCursor(dbHelper.fetchAllCities());
                cursorAdapter.notifyDataSetChanged();
            }
        }
        else if (rcode == RESULT_MAJ_CITY){
            dbHelper.updateCity((City) data.getParcelableExtra("city"));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

