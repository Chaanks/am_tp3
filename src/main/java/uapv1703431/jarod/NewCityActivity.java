package uapv1703431.jarod;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewCityActivity extends AppCompatActivity {

    private EditText textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);



        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String city = textCountry.getText().toString();
                String name = textName.getText().toString();

                if (city.isEmpty() || city.length() < 2) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                } else {
                    Intent intent = getIntent();
                    intent.putExtra("Country", city);
                    intent.putExtra("Name", name);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }

            }
        });
    }
}
